# reprolang

To complete the submission to REPROLANG 2020, and to be checked by a CLARIN panel afterwards, 
the software used to obtain the results reported in your paper must be made available 
as a docker container through a project in gitlab.

These are the guidelines offered by the CLARIN team to make your software available
to REPROLANG 2020.

To get further technical support, you can contact the CLARIN team
at reprolang-tc@clarin.eu or an issue can be created 
through [this](https://gitlab.com/CLARIN-ERIC/reprolang/issues) project page.

These guidelines assume you have basic functional knowledge 
of [Docker](https://docs.docker.com/), [git](https://git-scm.com/doc) 
and basic shell commands (we use [bash](https://www.gnu.org/software/bash/manual/bash.html)). 
If you are not familiar with some of these technologies, the linked pages 
are a good place to start.

### Requirements:
* Local Linux or OSX operating system.
* Local git client installed.
* Gitlab.com account (see [section 2](https://gitlab.com/CLARIN-ERIC/reprolang#2-create-a-private-project-in-gitlabcom-and-add-it-as-a-remote-to-your-local-project) for more details).
* Local docker daemon (see [section 4](https://gitlab.com/CLARIN-ERIC/reprolang#4-build-and-run-locally) for more details).

### 0. Recommended OS

The recommended OS is debian/ubuntu Linux.


### 1. Create a local git repository for your experiment

For each experiment concerning a given reproduction task in REPROLANG 2020, 
you should create a local git repository by following one of the two options
A or B below.

Option B avoids your executing of arbitrary code from the internet.


#### Option A

```
mkdir "${HOME}/${EXPERIMENT}"
cd "${HOME}/${EXPERIMENT}"
curl -s -L https://gitlab.com/CLARIN-ERIC/build-script/raw/reprolang/init_repo.sh | bash
```

Where:

* `$EXPERIMENT`: is the lower case (directory) name for the experiment and local git repository.

#### Option B
If you don’t trust executing arbitrary code from the internet (make sure to adjust the `VERSION` parameter):

```
mkdir "${HOME}/${EXPERIMENT}"
cd "${HOME}/${EXPERIMENT}"
VERSION=reprolang-1.0.0 && \
git init && \
git submodule add https://gitlab.com/CLARIN-ERIC/build-script.git build-script && \
cd build-script && \
git checkout ${VERSION} && \
cd .. && \
ln -s build-script/build.sh build.sh && \
ln -s build-script/copy_data_noop.sh copy_data.sh && \
ln -s build-script/update_version_noop.sh update_version.sh && \
cp build-script/_gitlab-ci_default.yml .gitlab-ci.yml && \
#
# Optional (to run locally)
mkdir input && \
mkdir -p output/tables_and_plots && \
mkdir output/datasets
```

Where:

* `$EXPERIMENT`: is the lower case (directory) name for the experiment and local git repository.


### 2. Create a private project in gitlab.com and add it as a remote to your local project

Create an acount on https://gitlab.com.

Create a new, private, repository for your experiment.

Run the following commands to add this new remote repository to your local experiment directory:

```
cd "${HOME}/${EXPERIMENT}"
git remote add origin ${GIT_REPO_URL}
```

Where:

* `$EXPERIMENT`: is the lower case (directory) name for the experiment and local git repository.
* `$GIT_REPO_URL`: is the HTTP clone location of the repository you created on gitlab.com.


### 3. Configure your docker project


#### 3.1 Entrypoint script
Create `${HOME}/${EXPERIMENT}/image/run.sh` as the main shell script (entrypoint in docker terminology) of your project, 
which must use these 3 locations inside your docker image:
- directory containing all input data sets: `/input` , which includes a subdirectory `/input/run_test` containing the test data sets to feed run.sh and obtain the task comparables (not applicable to reproduction task E.1).
- directory for output data sets: `/output/datasets`
- directory for output comparables, including scores, tables and/or plots, etc. (i.e. the major reproduction comparables of the respective task, indicated in the call for papers): `/output/tables_and_plots`
***
**NOTE**: These directories are not to be created inside the docker image. They will be supplied from the host system later on, when running the docker image. When these directories exist inside the image, they are overridden and their contents wiped at runtime.
***

```
$/ tree
.
├── input                   #directory containing all input data sets (actual data will be provided at runtime)
└── output                  #directory containing output ouput data
    ├── datasets            #directory for output data sets
    └── tables_and_plots    #directory for output comparables, including scores, tables and/or plots,
                            #etc. (i.e. the respective major reproduction comparables indicated in the call for papers)
```

When you run your experiment locally, these directories will map to those at the root of your experiment repository `${HOME}/${EXPERIMENT}/` (see: [section 1](https://gitlab.com/CLARIN-ERIC/reprolang#1-create-a-local-git-repository-for-the-experiment)).
More on this below (see: [section 4b](https://gitlab.com/CLARIN-ERIC/reprolang#4b-running-locally)).


#### 3.2 Dockerfile

Build the Dockerfile configured to run the experiment.

Create `${HOME}/${EXPERIMENT}/image/Dockerfile` with the following content:

```
#######################
# Choose an OS or runtime environment image that fits the needs of your experiment e.g.
FROM debian:jessie
#Or:
#FROM python:3.7.2
#######################

#Define input/output directories
VOLUME "/input"
VOLUME "/output/datasets"
VOLUME "/output/tables_and_plots"

#Add and set entrypoint
ADD run.sh /run.sh
RUN chmod u+x /run.sh
ENTRYPOINT /run.sh

#######################
# Customization start #
#######################

#Add any custom dependencies and/or scripts here

#######################
# Customization end   #
#######################
```

Update the customization section as needed for your project.


### 4. Build and run locally

Build and run the docker image locally to verify that the experiment runs as expected.

This requires the docker daemon is locally installed. Installation instructions:
* Docker installation instructions: https://docs.docker.com/install/
* If you run into permission issues when running the docker container locally, have a look at the instructions here: https://docs.docker.com/install/linux/linux-postinstall/

#### 4.a Building locally

```
cd "${HOME}/${EXPERIMENT}"	#Make sure you are in the root of the project dir
bash build.sh --build --local
```

Where:

* `$EXPERIMENT`: is the lower case (directory) name for the experiment and local git repository.

#### 4.b Running locally


Execute this command to run the experiment locally:

```
cd  ${HOME}/${EXPERIMENT}
docker run \
    -ti --rm --name=$PROJECT_NAME \
    -v ${PWD}/input:/input \
    -v ${PWD}/output/datasets:/output/datasets \
    -v ${PWD}/output/tables_and_plots:/output/tables_and_plots \
    <image name>:<image tag>
```

Where:

* `$PROJECT_NAME`: is a freely chosen, lowercase, name for the docker container (can then be used instead of the container id to reference the container).
* `$EXPERIMENT`: is the lower case (directory) name for the experiment and for the local git repository.
* `<image name>:<image tag>`: the image name equals the `$EXPERIMENT` value, and the image tag equals the identifier of the local git commit, potentialy concatenated
with the remote tag name (if already created in [section 6](https://gitlab.com/CLARIN-ERIC/reprolang#6-tag-a-new-release-to-trigger-ci-docker-image-creation)).
The last line of the output generated in [section 4a](https://gitlab.com/CLARIN-ERIC/reprolang#4a-building-locally) will show this values in the form: 
`Successfully tagged <image name>:<image tag>`

The experiment output will be locally available in the `${HOME}/${EXPERIMENT}/output/datasets` and `${HOME}/${EXPERIMENT}/output/tables_and_plots`.
***
**NOTE**: The local path of the 3 directories specified above does not necessarily have to point to `${HOME}/${EXPERIMENT}/[input|output]`. 
For instance, if you already have your `input` data in some directory in your system, e.g.`/some/directory/data`, 
you can run the experiment by directly mounting this path as the input directory inside the image:
```
docker run \
    -ti --rm --name=$PROJECT_NAME \
	-v /some/directory/data:/input \
	...
```
***


### 5. Commit the changes and push to the remote repository in GitLab.com

Commit the changes to the local git repository:
```
cd "${HOME}/${EXPERIMENT}"
git add .
git commit -m "<write a commit message here>"
```

Push the new commits to the remote git repository:
```
cd "${HOME}/${EXPERIMENT}"
#For the first push
git push --set-upstream origin master

#For any subsequent pushes, you can use the shorter:
#git push
```

Where:

* `$EXPERIMENT`: is the lower case (directory) name for the experiment and local git repository.


### 6. Tag a new release to trigger CI docker image creation

After tagging a new release in the gitlab.com repository, a CI/CD pipeline will be started, and upon pipeline's successfull completion, the final docker image will by pushed
to the docker image registry on gitlab.com.


### 7. Perform steps 4. to 6. until the submission deadline of the LREC shared task

Make sure the final image is successfully built and provides the expected output when run with the specified input dataset.
`Non running images will not be fixed!`


### 8. Submission

Submit via the START conference management system used by LREC 2020:
- url of your gitlab.com project
- commit hash and tag of the release to be reviewed
- url of the tar.gz with the datasets 
- the md5 checksum of the above tar.gz
- .pdf with the paper, which must include the above url of your gitlab.com project, and the above commit hash and tag


### 9. Publication

Make your project in gitlab.com public within 2 days after the submission deadline.


### Frequently Asked Questions

#### Too large archive error

If your GitLab CI build fails with the folling error: 
```
 too large archive  id=... responseStatus=413 Request Entity Too Large status=413 Request Entity Too Large ...
 ```
 
 This means your image exceeded the 1GB artifact limit set on gitlab.com. You can try to reduce the image size to <1GB.
 If this is not possible, we will build the image manually outside of the gitlab ci environment before running the experiment.
 Please make sure the image builds succesfully, otherwise we might not be able to run your experiment.
